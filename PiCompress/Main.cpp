#include "Main.h"

#include <cstdint>
#include <ctime>

#include <string>
#include <vector>
#include <chrono>
#include <atomic>
#include <thread>
#include <memory>
#include <iostream>
#include <iomanip>
#include <sstream>

#define BENCHMARK_MODE 1

const size_t kChunkSize = 1024;

std::atomic<bool> is_found_ = false;
std::atomic<size_t> start_ = 0;
std::atomic<size_t> result_ = 0;

#if BENCHMARK_MODE
const size_t kStop = 5;
const int kRuns = 5;
#endif

const std::string data = "J_";
std::vector<std::thread> threads;

uint32_t get_num_processors()
{
	uint32_t count = std::thread::hardware_concurrency();

	// In some cases this can return 0 if it cannot detect the number of processors
	if (count == 0) return 1;
	else return count;
}

std::string get_date_time()
{
	auto now = std::chrono::system_clock::now();
	auto now_c = std::chrono::system_clock::to_time_t(now);

	// Would use `std::localtime(&now_c)` except it's considered unsafe
	tm result;
	auto err = localtime_s(&result, &now_c);
	if (err) {
		std::cerr << "Failed to get local time for logging purposes" << std::endl;
		exit(1);
	}

	std::stringstream stream;
	stream << std::put_time(&result, "%c");
	return stream.str();
}

void worker_thread(int id)
{
	while (true) {
		size_t instance_count = start_.fetch_add(1, std::memory_order_relaxed);

#if BENCHMARK_MODE
		if (instance_count >= kStop) {
			break;
		}
#endif

		size_t instance_start = instance_count * kChunkSize;
		size_t instance_end = (instance_count + 1) * kChunkSize;

		auto start = std::chrono::high_resolution_clock::now();
		
		for (size_t i = instance_start; i < instance_end; i++) {
			for (size_t j = 0; j < data.length(); j++) {
				if (get_byte(i + j) != data[j]) { break; }

				if (j == data.length() - 1) {
					is_found_ = true;
					result_ = i;
					break;
				}
			}

			if (is_found_) { break; }
		}

		auto end = std::chrono::high_resolution_clock::now();
		auto diff = end - start;
		auto diff_ms = std::chrono::duration<double, std::milli>(diff).count();

		std::cout
			<< get_date_time()
			<< " [" << id << "]: "
			<< "Finished chunk #" << instance_count << " (" << instance_start << ") "
			<< "Took " << diff_ms << "ms"
			<< std::endl;


		if (is_found_) { break; }
	}
}

int main()
{
	const int num_precessors = get_num_processors();

#if BENCHMARK_MODE
std::vector<double> times__;

for (int k__ = 0; k__ < kRuns; k__++) {
	std::cout << "Run #" << k__ << std::endl;
	start_ = 0;
#endif

	//
	auto start = std::chrono::high_resolution_clock::now();
	//

	for (int i = 0; i < num_precessors; i++) {
		threads.push_back(std::thread(worker_thread, i));
	}

	for (auto &thread : threads) {
		thread.join();
	}

	threads.clear();

	//
	auto end = std::chrono::high_resolution_clock::now();
	auto diff = end - start;
	auto diff_ms = std::chrono::duration<double, std::milli>(diff).count();
	//

#if BENCHMARK_MODE
	times__.push_back(diff_ms);

	std::cout
		<< "Chunks: " << kStop - 1
		<< " Took:" << diff_ms << "ms"
		<< std::endl;

	continue;
#endif

	std::cout
		<< std::endl << "Index at: "
		<< result_
		<< " Took:" << diff_ms << "ms"
		<< std::endl;

#if BENCHMARK_MODE
}

double total = 0.0;
for (double& x : times__) {
	total += x;
}

double avg = total / kRuns;

std::cout
<< "Runs: " << kRuns
<< " Total:" << total
<< " Avg:" << avg
<< std::endl;
#endif

	std::cin.get();

	return 0;
}